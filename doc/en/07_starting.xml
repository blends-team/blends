<chapter id="starting">
  <title>How to start a Debian Pure Blend</title>
  
<para>
This chapter is based on the Debian Subproject HOWTO, which was
written by Ben Armstrong <email>synrg@debian.org</email>.
</para>

 <sect1 id="planning">
 <title>Planning to form a Debian Pure Blend</title>

  <para>
  In this section, issues to think about before starting a Debian Pure
  Blend will be discussed. It is important to have a clear idea where
  to head and how to get there before launching into this adventure.
  </para>

 <sect2 id="leadership">
 <title>Leadership</title>
  <para>
  The existing Debian Pure Blends have clearly shown that they depend
  on a person who keeps things running.  If anybody wants to start a
  project at first, he has to answer the question:
  <emphasis>"Am I the right person for the job?"</emphasis> Surely this is a
  question that may be faced with some amount of uncertainty.  The way
  Debian Pure Blends started in the past was for the person with the
  idea for the project to just start doing the work.  After some time
  using this approach, it became clear that if the project lacked a
  person to take leadership, the project would become stale.  So the
  initiator has to answer the question clearly, whether or not he is
  able to continue in the <emphasis>job</emphasis> of leader, considering the
  amount of time he will have to spend, and the technical and social
  skills which are needed.
  </para>
 </sect2>

 <sect2 id="defining_scope">
 <title>Defining the scope of the Blend</title>
  <para>
  It is as important to decide what your group is not going to do as
  it is what it is going to do.  A clear borderline is essential for
  the development of the project.  Without it, outsiders might
  either expect more from the project than it can accomplish, or may ignore
  the project, finding it not helpful because they are
  not able to find out the purpose.
  </para>
  <para>
  By maintaining a good relationship with other Free Software projects,
  some common tasks can be done very effectively.  When efforts can be
  shared, the amount of work for each project can be reduced.
  </para>
  <para>
  Checking for cooperation with other Debian Pure Blends is always a
  good idea.  In technical terms, this is obvious, but sometimes there
  are possibilities to share efforts when the goals of two projects
  have parts in common.
  </para>
  <para>
  The one who decides to start a Debian Pure Blend takes on a
  responsibility for this project.  It has to be for the good of
  Debian as a whole, and should bring an extra reputation to our
  common goal to build the best operating system.
  </para>
 </sect2>

 <sect2 id="initial_discussion">
  <title>Initial discussion</title>
  <para>
  By the time you have begun to think about forming the subproject,
  have made the commitment to lead it, and have sketched out a
  bit of where you want to go and how you'll get there, you have
  likely already done some informal discussion with your peers.
  It is time, if you haven't already, to take these ideas to the
  broader Debian developer community, opening discussion on the
  creation of your group.
  </para>

 <sect3 id="calling_all_developers">
  <title>Calling all developers</title>
  <para>
  At this stage, you will want to reach as broad an audience as possible.
  You have carefully thought out what you're going to do, and are able
  to articulate it to Debian as a whole.  Let everyone know through
  the <email>debian-devel-announce@lists.debian.org</email> mailing
  list, setting the <emphasis>Reply-to:
  </emphasis><email>debian-devel@lists.debian.org</email> and listen to what
  everyone has to say about your idea.  You may learn some valuable
  things about pitfalls that may lie ahead for your group.  Maybe even
  show-stoppers at that.  You may also find a number of like-minded
  individuals who are willing to join your group and help get it
  established.
  </para>
 </sect3>

 <sect3 id="steering_the_discussion">
  <title>Steering the discussion</title>
  <para>
  It's all too easy to get lost in ever-branching-out sub-threads at
  this point.  Many people will be firing off ideas left, right and
  centre about what your group should do.  Don't worry too much about
  containing the discussion and keeping it on track with your main
  idea.  You would rather not squelch enthusiasm at this point.  But
  do try to steer the discussion a bit, focusing on the ideas that
  are central to your subproject and not getting lost in the details.
  </para>
  <para>
  At some point, you'll decide you've heard enough, and you're ready
  to get down to the business of starting your group.
  </para>
 </sect3>

 </sect2>

</sect1>

<sect1 id="setting_up">
  <title>Setting up</title>

<sect2>
  <title>Mailing list</title>
  <para>
  It is fairly important to enable some means for communication for
  the project.  The most natural way to do this is with a mailing
  list.
  </para>
  <para>
  Creating a new mailing list starts with a wishlist bug against
  <package>lists.debian.org</package>.  The format of this
  bug has to follow
  <ulink url="http://www.debian.org/MailingLists/HOWTO_start_list">certain rules</ulink>.
  </para>
  <para>
  Before your list can be created, the listmasters will want assurance
  that creation of the list is, in fact, necessary.  So for this
  reason, don't wait for your list to be created.  Start discussing
  your new project on <email>debian-devel@lists.debian.org</email>
  immediately.  To help distinguish your project's posts from the
  large amount of traffic on this list, tag them in the Subject field
  with an agreed-upon tag. An example bug report to create the
  relevant list is bug
  <ulink url="http://bugs.debian.org/237017">#237017</ulink>.
  </para>
  <para>
  When sufficient discussion on the developer's list has
  taken place and it is time to move it to a subproject list,
  add to your wishlist bug report some URLs pointing to these
  discussions in the archives as justification for creation of
  your list.
  </para>
</sect2>

<sect2>
  <title>Web space</title>
  <para>
  A simple possibility, and one which is fairly attractive because it
  facilitates collaborative web site creation and maintenance, is to
  put a page on the <ulink url="http://wiki.debian.org">Wiki</ulink>.
  There is a
  special <ulink url="http://wiki.debian.org/index.cgi?DebianPureBlends">
  Wiki page for Debian Pure Blends</ulink>.
  </para>
  <para>
  A good place to put static web pages is the common
  place for all
  Blends: <ulink url="http://blends.debian.org"/>.  There is a
  subdirectory for each Blend and it is very easy to create a simple
  index page there which points to the automatically generated web
  pages which are mentioned in <xref linkend="web_if"/>.  Following this
  strategy is quite cheap and has a big effect when using the tools
  provided by the Debian Pure Blends effort.
  </para>
  <para>
  Sooner or later a Debian Pure Blend will establish an own project at
  <ulink url="https://salsa.debian.org">salsa.debian.org</ulink> to host
  own Git repositories.
  </para>
  <para>
  Finally, the best way is to have a page
  under <ulink url="http://www.debian.org/devel"/>.  While not as
  straightforward as any of the other options, this approach has its
  advantages.  First, the site is mirrored everywhere.  Second, the
  Debian web site translators translate pages into many different
  languages, reaching new potential audiences for your Debian Pure
  Blend, and improving communication with other members of your
  project and interested parties for whom English is not their most
  comfortable language.  Third, a number of templates are available to
  make your site more integrated with the main web site, and to assist
  with incorporating some dynamic content into your site. Before you
  join the Debian Web team you
  should <ulink url="http://www.debian.org/devel/website">learn
  more about building Debian web pages</ulink>.
  </para>
  <para>
  Once this is done, the Debian web pages team should be contacted via
  the mailing list <email>debian-www@lists.debian.org</email> to add the
  project to the <ulink url="http://www.debian.org/intro/organization">organisation page</ulink>.
  </para>
</sect2>

<sect2>
  <title>Repository</title>
  <para>
    On <ulink url="https://salsa.debian.org/">salsa.debian.org</ulink> a
    particular
  <ulink url="https://gitlab.com/">GitLab</ulink> instance is running to host
  all Debian related project work.  Creating a project on Salsa is a
  good idea to start teamwork on the code a Debian Pure Blend is
  releasing.
  </para>
</sect2>

<sect2>
  <title>Formal announcement</title>
  <para>
  Once there is a list, or at least enough preliminary discussion on
  debian-devel to get started, and there is some information about the
  newly planned Debian Pure Blend available on the web, it is time to
  send a formal announcement to
  <email>debian-devel-announce@lists.debian.org</email>.  The
  announcement should include references to past discussions, any web
  pages and code which might already exist, and summarise in a well thought
  out manner what the project is setting out to achieve.  Enlisting the
  help of fellow developers on irc or in private email to look over
  the draft and work out the final wording before it is sent out is
  always a good idea.
  </para>
  <para>
  Emails to <email>debian-devel-announce@lists.debian.org</email> have to
  be signed by the GPG key of an official Debian developer.  However, it
  should not be a very hard task if somebody wants to support Debian
  while not yet being a developer to find a developer who volunteers
  to sign an announcement of a reasonable project.  It might be
  reasonable to send this announcement also to other relevant non-Debian
  lists.  If your announcement is well done, it will draw a number
  of responses from many outsiders, and will attract people to Debian.
  </para>
</sect2>

<sect2>
  <title>Explaining the project</title>
  <para>
  Now the real work starts.  People who are involved in the project
  should be aware that they have to answer questions about the project
  whenever they show up at conferences or at an exhibition booth.  So
  being prepared with some flyers or posters is always a good idea.
  </para>
</sect2>
</sect1>

<sect1 id="structure">
  <title>Project structure</title>

<sect2 id="subsetting_debian">
  <title>Sub-setting Debian</title>
  <para>
  While there are a variety of different kinds of work to be done in
  Debian, and not all of them follow this pattern, this document
  describes one particular kind of project.  Our discussion about
  Debian Pure Blends concerns sub-setting Debian.  A sub-setting
  project aims to identify, expand, integrate, enhance, and maintain a
  collection of packages suitable for a particular purpose by a
  particular kind of user.
  </para>
  <para>
  Now, strictly speaking, a subset of packages could be more general
  than described above.  A subset could be a broad category like
  "audio applications" or "network applications".  Or it could be more
  specific, such as "web browsers" or "text editors".  But what a
  sub-setting project such as Debian Jr. aims to do is not focus on the
  kind of package, but rather the kind of user.  In the case of Debian
  Jr. it is a young child.
  </para>
  <para>
  The sort of user the project looks after, and which of the needs the
  project hopes to address are defined by the project's goals.
  <remark>BA: I had a bit of trouble deciding how to punctuate the following
  passage.  I considered and rejected the advice given here in response to
  Kirsten's question about punctuating a list of questions:
  http://www.udel.edu/eli/g20.html, instead following the advice found
  elsewhere on the Web that double punctuation should be avoided.</remark>
  Thus, Debian Jr. first had to decide which children the project
  would reach: "What age?" "English speaking children only, or
  other languages as well?"  Then the project had to determine
  how and where they would be using Debian: "At home?" "In school?"
  "Playing games?" "On their own systems?" "On their parents' systems?"
  </para>
  <para>
  The answers to all of these questions are not straightforward.  It is
  very much up to the project to choose some arbitrary limits for the
  scope of their work.  Choose too broad a focus, or one which duplicates
  work already done elsewhere, and the energy of the project dissipates,
  making the project ineffective.  Choose too narrow a focus and the
  project ends up being marginal, lacking the critical mass necessary
  to sustain itself.
  </para>
  <para>
  A good example was the request to split the microbiology
  related packages out of Debian Med into a Debian Bio project.  This
  is reasonable in principle, and should really be done.  In fact, the
  initiator of Debian Med would support this idea.  So he gave the
  answer: "Just start the Debian Bio project to take over all related
  material.  Until this happens, Debian Med will cover medical material
  that deals with sequence analysis and so forth."  Unfortunately,
  there was silence from the Debian Bio proponents after this answer.
  </para>
  <para>
  Of course, it sometimes turns out that you start working on a project
  thinking you know what it is about, only to find out later that you
  really had no idea what it would become until the user base has grown
  beyond the small community of developers that started it.  So, none of
  the decisions you make about your project's scope at the beginning
  should be taken as set in stone.  On the other hand, it is your project,
  and if you see it veering off in directions that are contrary to your
  vision for it, by all means steer it back on course.
  </para>
</sect2>

<sect2 id="tasksel">
 <title>Using tasksel and metapackages</title>
<para>
  According to the plan of the project, the first metapackages (<xref
  linkend="metapackages"/>) should be developed.  It is not always easy to
  decide what should be included, and which metapackages should be
  built. The best way to decide on this point is to discuss on the
  mailing list some well thought out proposals.
</para>
<para>
  Section <xref linkend="text_ui"/> mentions <package>tasksel</package> as a tool
  to select a Debian Pure Blend, and explains why it is currently not
  possible to get a Blend included into the task selection list.
</para>
  </sect2>

<sect2 id="normalpackages">
 <title>Adding new "normal" packages</title>
<para>
  Besides metapackages, you may want to develop some new packages for your blend
  in order to customize some system behavior, like changing XDG menu style.  Those
  normal packages are not defined in your tasks, hence we need to define it properly
  so that blends-dev will know how to package these new, normal packages.  Adding
  new normal packages is almost identical to debian packaging, just that you need
  to define your package parameters in debian/control.stub instead of debian/control.
  Section <xref linkend="developing_normal_packages"/> describes what you need to know for
  adding new normal packages in Debian Pure Blends in more detail.
</para>
</sect2>

</sect1>

<sect1 id="first_release">
  <title>First release</title>

<sect2 id="release_announcement">
  <title>Release announcement</title>
  <para>
  Beyond the release announcement for Debian itself, it is necessary
  to put some thought and work into a release announcement for the
  first release of a Debian Pure Blend.  This will not only
  be directed at the Debian developer community, but also at the users.
  This will include potential new Debian users abroad, who may not be
  on a Debian mailing list.  Here, the same principle applies as for
  the first announcement of the project: it is important to consider
  sending the information to other relevant forums.
  </para>
</sect2>

<sect2 id="users">
  <title>Users of a Debian Pure Blend</title>
  <para>
  By this time, people have newly installed Debian along with the
  material in the Blend, or have installed the metapackages on their
  existing Debian systems.  Now comes the fun part, building
  relationships with the user community.
  </para>

<sect3 id="user_support">
  <title>Devoting resources to the users</title>
  <para>
  Users are a mixed blessing.  In the first development phase there are
  some developers who are users, and some intrepid "early adopters."
  But once it is released, the first version is "out there," and the
  project will certainly attract all kinds of users who are not
  necessarily as technically savvy as your small development user
  community.  Be prepared to spend some time with them.  Be patient
  with them.  And be listening carefully for the underlying questions
  beneath the surface questions.  As draining as it can be to deal
  with users, they are a very key component to keeping your
  development effort vital.
  </para>
</sect3>

<sect3 id="devel_vs_user_list">
  <title>Developer vs. user mailing list</title>
  <para>
  Should a user list be created?  It's not as cut-and-dried as it
  might at first appear.  When user help requests start coming in,
  you might at first see them as a distraction from the development
  effort.  However, you don't necessarily want to "ghettoize" the
  user community into a separate list early.  That's a
  recipe for developers to get out of touch very quickly with the
  users.  Tolerate the new user questions on the developer list
  for a while.  Once a user list is finally set up, courteously
  redirect user questions to the user list.  Treat your users as
  the valuable resource about how your project is working "in the
  field" that they are.
  </para>
</sect3>

<sect3 id="user_support_beyond_debian">
  <title>User support beyond Debian</title>
  <para>
  Fortunately, we're not in the business of supporting users alone.
  Look beyond Debian for your allies in user support: Linux user
  groups (LUGs) and the users themselves.  Develop an awareness of who
  has stakes in seeing your project succeed, and enlist their help
  in getting a strong network of support established for your work.
  </para>
</sect3>

</sect2>

</sect1>

</chapter>
