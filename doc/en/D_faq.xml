  <appendix id="FAQ">
  <title>FAQ</title>

  <sect1 id="AddDep">
  <title>How can I add a dependency?</title>

<para>
If the source for the Blends package is maintained in Blends VCS every Debian
developer and each member of the Alioth team "Debian Pure Blends" have commit
permission to the package source.  Once you checked it out you find the single
tasks files inside the <filename>tasks/</filename> directory.  Fine one or
more tasks you want to put the package into and add the line.
<informalexample>
  <programlisting>
Depends: <varname>package</varname>
  </programlisting>
</informalexample>
or
<informalexample>
  <programlisting>
Suggests: <varname>package</varname>
  </programlisting>
</informalexample>
</para>
   </sect1>

   <sect1 id="Additional">
   <title>What additional information should be provided?</title>

<para>
As explained in <xref linkend="edittasksfiles"/> you can specify several metadata
in addition to the pure dependency which is used for the metapackage creation
to enrich the web sentinel with extra information about the package.  Usually
the information is obtained from the Debian package information.  However,
as it was explained it makes perfectly sense to even specify packages which
are not yet included into official Debian.  The information is taken from the
<ulink url="http://wiki.debian.org/UltimateDebianDatabase">Ultimate Debian
Database (UDD)</ulink>
</para>
<para>
Since the Blend team also has injected the packages in the NEW queue as
well as the information about packages in VCS of Blends it is sufficient
even for packages which are not yet ready to only specify the name.  In
this case it is detected that the package is work in progress and the
tasks page of the web sentinel will put the package into the appropriate
section.
</para>
<para>
The big advantage of injecting a package into the relevant task is that
visitory of the tasks page will be informed about the work in progress and
can follow for instance enhancements of the desciprion or the migration
of the package into the Debian pool without any further action.  Since this
is very convenient you want to add your package to the tasks immediately
after you injected the first rough packaging into VCS.
</para>
   </sect1>

   <sect1 id="BinaryPackage">
   <title>Should I add binary or source packages?</title>

<para>
A tasks file is in principle a <filename>debian/control</filename> file
snippet.  In these files you are specifying <emphasis>binary</emphasis>
packages.  It is a common error to inject source package names which is
wrong if the resulting package is different.
</para>
   </sect1>

   <sect1 id="Library">
   <title>Should I add a library package to a user task?</title>

<para>
User oriented metapackages should depend from user oriented applications.
You should avoid specifying a package containing a dynamic library.  If
the package you want to add is a library you should rather add the
development package (containing the static library and header files)
to the according development task if this exists.
</para>

   </sect1>

   <sect1 id="NewTask">
   <title>Can I create a new task if the existing ones do not fit?</title>

<para>
Yes.  Please discuss this with the Blends team on their mailing list but
in principle it is totally OK to add a new task to a Blend.
</para>

   </sect1>
   <sect1 id="Wiki">
   <title>Why not simply use a Wiki?</title>

<para>
People frequently claim that they prefer a simple list in a Wiki to list the
interesting packages for their work.  This is a waste of time since:
</para>
<para>
<orderedlist>
  <listitem><para>
     An entry in a tasks file makes the same effort (measured in time and
     numbers of keys pressed).
  </para></listitem>
  <listitem><para>
     You need to manually add the metadata (like Homepage, description etc.)
  </para></listitem>
  <listitem><para>
     Metadata are updated automatically in the web sentinel tasks - Wikis are
     usually aging (despite being a Wiki)
  </para></listitem>
  <listitem><para>
     You get a lot of metadata for free:
     <itemizedlist>
       <listitem><para>
         <emphasis>Translated</emphasis> descriptions
       </para></listitem>
       <listitem><para>
         Version + release
       </para></listitem>
       <listitem><para>
         Popularity contest results
       </para></listitem>
       <listitem><para>
         Debtags
       </para></listitem>
       <listitem><para>
         Screenshot
       </para></listitem>
     </itemizedlist>
     You will have a hard time to provide all this on a Wiki and keep it up to date
  </para></listitem>
  <listitem><para>
    Automatic information whether Debian is lagging begind upstream
  </para></listitem>
  <listitem><para>
    If a package was introduced into Debian after the work in VCS was successfully
    finished the web sentinel is updated automatically without any extra work.
  </para></listitem>
  <listitem><para>
    If you want to create metapackages for user installation or to easily feed some
    live DVD you need to create tasks files anyway and thus you are not duplicating
    any work.
  </para></listitem>
</orderedlist>
</para>
   </sect1>

   <sect1 id="DebTags">
   <title>Why not simply using DebTags?</title>
<para>
In general I'd like to quote Enrico Zini about the relation of DebTags
and Blends tasks:  "Both efforts are orthogonal to each other and a well
designed task should cover a specific workfield which is not necessarily
given due to the fact that a package belongs to a certain (DebTag)
category."
</para><para>
Moreover if you exclusively rely on the DebTags you are ignoring additional
options the Blends framework is providing.  For instance the 
<ulink url="http://blends.debian.org/med/tasks/bio">Biology task of Debian Med</ulink>
contains several sections about packages which are not yet available in the
Debian package pool.  This includes:
<orderedlist>
  <listitem><para>
    Packages inside the new queue
  </para></listitem>
  <listitem><para>
    Packages the team is working on in VCS
  </para></listitem>
  <listitem><para>
    Unofficial packages at some other places
  </para></listitem>
  <listitem><para>
    Information about not yet packaged software that might be of certain interest
  </para></listitem>
</orderedlist>
This turns out as a very useful todo list to grab work items from as well
as information to prevent duplication of work.  (Yes ITP bugs do not always
work out that reliably.)
</para><para>
The Blends framework works quite nicely automatically if packages "upgrade"
from one section to the other which means if you specify a package which has
only some packaging in VCS it is sufficient to simply add a dependency to
the tasks file and it shows up
<ulink url="http://blends.debian.org/med/tasks/bio#pkgvcs-debs">here</ulink>
(to stick to our example above).  So all relevant information is just taken
from VCS and if you for instance change the description in your packaging
the tasks page will be updated automatically (after some cron job delay).
</para>
<para>
Once the package might be uploaded the first time it migrates to the
section named "Debian packages in New queue (hopefully available soon)"
(which is only available if there are such packages) and finally once
ftpmaster might have accepted the package it shows up in the tasks ...
even not yet DebTagged.  You have another delay until the DebTag information
is propagated properly and thus relying on DebTags in this whole process
does not work.
</para>
   </sect1>

  </appendix>

